
import nt.cs.arn.encoglogicgates.AND;
import nt.cs.arn.encoglogicgates.OR;
import nt.cs.arn.encoglogicgates.LogicGate;
import nt.cs.arn.encoglogicgates.XNOR;
import nt.cs.arn.encoglogicgates.XOR;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import nt.cs.arn.utils.Point;
import nt.cs.arn.utils.Utils;

/**
 *
 * @author arnaldo_neto
 */
public class Main {
    private static final String BASE_PATH = System.getProperty("user.home") + '/';

    public static void main(String[] args) throws IOException {
        LogicGate orGate;
        LogicGate andGate;
        LogicGate xorGate;
        LogicGate xnorGate;

        do {
            orGate = new OR();
            orGate.doTtraining();
        } while (!orGate.successful());

        do {
            andGate = new AND();
            andGate.doTtraining();
        } while (!andGate.successful());

        do {
            xorGate = new XOR();
            xorGate.doTtraining();
        } while (!xorGate.successful());

        do {
            xnorGate = new XNOR();
            xnorGate.doTtraining();
        } while (!xnorGate.successful());

        writeResult(orGate, "or");
        writeResult(andGate, "and");
        writeResult(xorGate, "xor");
        writeResult(xnorGate, "xnor");
    }

    private static void writeResult(LogicGate gate, String gateName) throws IOException {
        BufferedImage image = new BufferedImage(401, 401, BufferedImage.TYPE_INT_BGR);
        for(int i = 0; i <= 400; ++i) {
            for(int j = 0; j <= 400; ++j) {
                double x = i / 400.0;
                double y = j / 400.0;

                double value = gate.doCompute(x, y);

                final int red = (int) Utils.interpolate(value, new Point[] {new Point(0, 255), new Point(0.1, 255), new Point(0.2, 192), new Point(0.8, 192), new Point(0.9, 0), new Point(1, 0)});
                final int green = (int) Utils.interpolate(value, new Point[] {new Point(0, 0), new Point(0.1, 0), new Point(0.2, 192), new Point(0.8, 192), new Point(0.9, 192), new Point(1, 192)});
                final int blue = (int) Utils.interpolate(value, new Point[] {new Point(0, 0), new Point(0.1, 0), new Point(0.2, 192), new Point(0.8, 192), new Point(0.9, 0), new Point(1, 0)});
                
                Color color = new Color(red, green, blue);

                image.setRGB(i, 400 - j, color.getRGB());
            }
        }

        for (double[] point: gate.getTrainingData()) {
            if(0 <= point[0] && point[0] <= 1 && 0 <= point[1] && point[1] <= 1) {
                if (point[2] > 0.9) {
                    image.setRGB((int) (point[0] * 400), (int) ((1 - point[1]) * 400), new Color(0, 127, 0).getRGB());
                } else if (point[2] < 0.1) {
                    image.setRGB((int) (point[0] * 400), (int) ((1 - point[1]) * 400), Color.YELLOW.getRGB());
                }
            }
        }

        BufferedImage mainView = new BufferedImage(800, 600, BufferedImage.TYPE_INT_RGB);
        final Graphics graphics = mainView.getGraphics();
        graphics.setColor(new Color(242, 242, 242));
        graphics.fillRect(0, 0, 800, 600);
        graphics.drawImage(image, 99, 99, null);
        graphics.setColor(Color.BLACK);
        graphics.drawLine(89, 500, 550, 500);
        graphics.drawLine(99, 510, 99, 49);
        graphics.drawLine(500, 490, 500, 510);
        graphics.drawLine(89, 99, 109, 99);
        Graphics2D graphics2D = (Graphics2D) graphics;
        graphics2D.drawString("1.0", 65, 105);
        graphics2D.drawString("0.0", 65, 505);
        graphics2D.drawString("0.0", 90, 525);
        graphics2D.drawString("1.0", 490, 525);
        graphics2D.drawString("X", 560, 505);
        graphics2D.drawString("Y", 95, 35);
        graphics2D.drawString(gateName, 650, 300);

        ImageIO.write(mainView, "png", new File(BASE_PATH + gateName + ".png"));
    }
}