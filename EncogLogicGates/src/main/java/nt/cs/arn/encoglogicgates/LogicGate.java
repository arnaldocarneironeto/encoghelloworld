package nt.cs.arn.encoglogicgates;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.encog.Encog;
import org.encog.ml.data.MLData;
import org.encog.ml.data.MLDataPair;
import org.encog.ml.data.MLDataSet;
import org.encog.ml.data.basic.BasicMLDataSet;
import org.encog.neural.networks.BasicNetwork;
import org.encog.neural.networks.training.propagation.resilient.ResilientPropagation;

/**
 *
 * @author arnaldo_neto
 */
public abstract class LogicGate {
    protected static final Random RANDOM = new SecureRandom();

    protected static final int TRAINING_DATA_LENGTH = 512;
    private static final double TARGET_ERROR = 0.001;

    protected double input[][] = new double[TRAINING_DATA_LENGTH][2];
    protected double ideal[][] = new double[TRAINING_DATA_LENGTH][1];

    protected BasicNetwork network;

    private boolean wasSuccessful = false;

    public void doTtraining() {
        MLDataSet trainingSet = new BasicMLDataSet(input, ideal);
        
        final ResilientPropagation train = new ResilientPropagation(network, trainingSet);

        int epoch = 1;
        
        do {
            train.iteration();
            System.out.println("Epoch #" + epoch++ + " Error: " + train.getError());
        } while (train.getError() > TARGET_ERROR && epoch < 10000);
        wasSuccessful = train.getError() <= TARGET_ERROR;
        train.finishTraining();

        System.out.println("Neural Network Results:");
        for(MLDataPair pair: trainingSet ) {
            final MLData output = network.compute(pair.getInput());
            System.out.println(String.format("%,7.4f", pair.getInput().getData(0)) + "," + String.format("%,7.4f", pair.getInput().getData(1))
                    + ", actual=" + String.format("%6.2f", output.getData(0)) + ",ideal=" + pair.getIdeal().getData(0));
        }

        Encog.getInstance().shutdown();
    }

    public double doCompute(double x, double y) {
        double[] singleInput = {x , y};
        double[] singleOutput = new double[1];

        network.compute(singleInput, singleOutput);
        
        return singleOutput[0];
    }
    
    protected final void setTrainingData(int index, double x, double y, double expectedResult) {
        input[index][0] = x;
        input[index][1] = y;
        ideal[index][0] = expectedResult;
    }

    protected static final double noise() {
        return (RANDOM.nextDouble() - RANDOM.nextDouble()) * 0.05;
    }

    public boolean successful() {
        return wasSuccessful;
    }

    public List<double[]> getTrainingData() {
        List<double[]> trainingData = new ArrayList<>();
        for(int i = 0; i < TRAINING_DATA_LENGTH; ++i) {
            trainingData.add(new double[] {input[i][0], input[i][1], ideal[i][0]});
        }
        return trainingData;
    }
}