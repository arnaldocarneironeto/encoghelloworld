package nt.cs.arn.encoglogicgates;

import org.encog.engine.network.activation.ActivationReLU;
import org.encog.engine.network.activation.ActivationSigmoid;
import org.encog.neural.networks.BasicNetwork;
import org.encog.neural.networks.layers.BasicLayer;

/**
 *
 * @author arnaldo_neto
 */
public class XOR extends LogicGate {
    public XOR() {
        for(int i = 0; i < TRAINING_DATA_LENGTH; ++i) {
            switch (RANDOM.nextInt(4)) {
                case 0:
                    setTrainingData(i, 0.0 + noise(), 0.0 + noise(), 0.0);
                    break;
                case 1:
                    setTrainingData(i, 1.0 + noise(), 0.0 + noise(), 1.0);
                    break;
                case 2:
                    setTrainingData(i, 0.0 + noise(), 1.0 + noise(), 1.0);
                    break;
                default:
                    setTrainingData(i, 1.0 + noise(), 1.0 + noise(), 0.0);
            }
        }

        network = new BasicNetwork();
        network.addLayer(new BasicLayer(null, true, 2));
        network.addLayer(new BasicLayer(new ActivationReLU(), true, 2));
        network.addLayer(new BasicLayer(new ActivationSigmoid(), false, 1));
        network.getStructure().finalizeStructure();
        network.reset();
    }
}