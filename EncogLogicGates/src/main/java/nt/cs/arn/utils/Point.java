package nt.cs.arn.utils;

/**
 *
 * @author arnaldo
 */
public class Point {
    private final double x;
    private final double y;

    public Point () {
        this(0);
    }

    public Point (double x) {
        this(x, 0);
    }

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }
}