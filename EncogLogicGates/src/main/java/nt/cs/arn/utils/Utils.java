package nt.cs.arn.utils;

import java.util.List;

/**
 *
 * @author arnaldo
 */
public class Utils {
    public static double interpolate(double x, Point[] f) {
        return interpolate(x, List.of(f));
    }

    public static double interpolate(double x, List<Point> f) {
        if (x <= f.get(0).getX()) return f.get(0).getY();
        if (x >= f.get(f.size() - 1).getX()) return f.get(f.size() - 1).getY();

        int i = 0;
        double x0;
        double y0;
        double x1;
        double y1;
        do {
            x0 = f.get(i).getX();
            y0 = f.get(i).getY();
            x1 = f.get(i + 1).getX();
            y1 = f.get(i + 1).getY();
            i++;
        } while (!(x0 < x && x < x1) && i < f.size() - 1);

        return y0 + (x - x0) / (x1 - x0) * (y1 - y0);
    }
}