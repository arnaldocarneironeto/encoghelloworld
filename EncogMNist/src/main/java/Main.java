import java.io.IOException;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Random;
import nt.cs.arn.encogmnist.ImageData;
import nt.cs.arn.encogmnist.MNistDataReader;
import nt.cs.arn.encogmnist.MNistImageNetwork;

/**
 *
 * @author arnaldo
 */
public class Main {
    private static final Random RANDOM = new SecureRandom();

    public static void main(String[] args) throws IOException {
        MNistDataReader trainingReader = new MNistDataReader("mnist_train.csv");
        MNistDataReader testReader = new MNistDataReader("mnist_test.csv");

        MNistImageNetwork network = new MNistImageNetwork(trainingReader);

        network.load();
//        network.doTtraining();

        double[][] testNormalizedData = testReader.getNormalizedData();
        double[][] testIdealLabels = testReader.getIdeals();

        for(int i = 0; i < 100; ++i) {
            int index = RANDOM.nextInt(testNormalizedData.length);

            double[] calculated = network.doCompute(testNormalizedData[index]);
            double[] ideal = testIdealLabels[index];

            int label = 0;
            int idealLabel = 0;
            for(int j = 0; j < 10; ++j) {
                if(calculated[j] > calculated[label]) label = j;
                if(ideal[j] > ideal[idealLabel]) idealLabel = j;
            }
            int[] percents = new int[10];
            for (int j = 0; j < 10; ++j) percents[j] = (int) (100 * calculated[j]);
            ImageData image = new ImageData(testNormalizedData[index]);
            if (calculated[label] > 0.7) {
                if (label == idealLabel) {
                    image.writeToDisk("outputs/correto/image" + RANDOM.nextInt(3000) + " - correto " + label + ".png");
                } else {
                    image.writeToDisk("outputs/errado/image" + RANDOM.nextInt(3000) + " - ideal " + idealLabel + " - probabilidades em porcento " + Arrays.toString(percents) + ".png");
                }
            }
            else {
                image.writeToDisk("outputs/não reconhecido/image" + RANDOM.nextInt(3000) + " - não reconhecido" + " - probabilidades em porcento " + Arrays.toString(percents) + ".png");
            }
        }
    }
}