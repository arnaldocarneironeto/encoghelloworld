package nt.cs.arn.encogmnist;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 *
 * @author arnaldo
 */
public class ImageData {
    private static final int WIDTH = 28;
    private static final int HEIGHT = 28;

    private final double[] data;
    private final double[] normalizedData;
    private final int label;

    public ImageData(double[] data, int label) {
        this.data = new double[data.length];
        System.arraycopy(data, 0, this.data, 0, data.length);

        this.normalizedData = new double[data.length];
        for(int i = 0; i < data.length; ++i) {
//            normalizedData[i] = data[i] / 127.5 - 1;
            normalizedData[i] = data[i] / 255.0;
        }

        this.label = label;
    }

    public ImageData(double[] normalizedData) {
        this.normalizedData = new double[normalizedData.length];
        System.arraycopy(normalizedData, 0, this.normalizedData, 0, normalizedData.length);

        this.data = new double[normalizedData.length];
        for(int i = 0; i < normalizedData.length; ++i) {
//            this.data[i] = (1 + normalizedData[i]) * 127.5;
            this.data[i] = normalizedData[i] * 255.0;
        }

        this.label = -1;
    }

    public ImageData(BufferedImage image) {
        BufferedImage resizedImage = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_BYTE_GRAY);
        final Graphics graphics = resizedImage.getGraphics();
        graphics.drawImage(image, 0, 0, WIDTH, HEIGHT, null);
        WritableRaster raster = resizedImage.getRaster();

        this.data = new double[WIDTH * HEIGHT];
        this.normalizedData = new double[WIDTH * HEIGHT];

        for(int i = 0; i < WIDTH; ++i) {
            for(int j = 0; j < HEIGHT; ++j) {
                this.data[j * WIDTH + i] = raster.getSample(i, j, 0);
//                this.normalizedData[j * WIDTH + i] = this.data[j * WIDTH + i] / 127.5 - 1;
                this.normalizedData[j * WIDTH + i] = this.data[j * WIDTH + i] / 255.0;
            }
        }

        this.label = -1;
    }

    public double[] getData() {
        return data;
    }

    public double[] getNormalizedData() {
        return normalizedData;
    }

    public int getLabel() {
        return label;
    }

    public final BufferedImage getImage() {
        BufferedImage image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_BYTE_GRAY);
        WritableRaster raster = image.getRaster();
        for(int i = 0; i < WIDTH; ++i) {
            for(int j = 0; j < HEIGHT; ++j) {
                int pos = j * WIDTH + i;
                raster.setSample(i, j, 0, data[pos]);
            }
        }
        return image;
    }

    public void writeToDisk(String path) {
        try {
            ImageIO.write(getImage(), "png", new File(path));
        } catch (IOException ex) {
            Logger.getLogger(ImageData.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}