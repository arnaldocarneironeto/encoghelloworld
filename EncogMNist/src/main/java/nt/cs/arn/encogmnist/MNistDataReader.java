package nt.cs.arn.encogmnist;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author arnaldo
 */
public class MNistDataReader {
    private static final int WIDTH = 28;
    private static final int HEIGHT = 28;
    private static final int DATA_LENGTH = WIDTH * HEIGHT;

    private final List<ImageData> imagesData = new ArrayList<>();

    public MNistDataReader(String path) throws IOException {
        Scanner scanner = new Scanner(new File(path));
        scanner.nextLine(); // Reads and ignores title line
        while(scanner.hasNextLine()) {
            double[] data = new double[DATA_LENGTH];
            String line = scanner.nextLine();
            String[] values = line.split(",");
            
            for(int i = 0; i < DATA_LENGTH; ++i) {
                data[i] = Integer.parseInt(values[i + 1]);
            }
            
            imagesData.add(new ImageData(data, Integer.parseInt(values[0])));
        }
    }

    public List<ImageData> getImagesData() {
        return imagesData;
    }

    public double[][] getData() {
        double[][] result = new double[imagesData.size()][imagesData.get(0).getData().length];

        for(int i = 0; i < imagesData.size(); ++i) {
            double[] imageData = imagesData.get(i).getData();
            result[i] = imageData;
        }

        return result;
    }

    public double[][] getNormalizedData() {
        double[][] result = new double[imagesData.size()][imagesData.get(0).getData().length];

        for(int i = 0; i < imagesData.size(); ++i) {
            double[] nomalizedImageData = imagesData.get(i).getNormalizedData();
            result[i] = nomalizedImageData;
        }

        return result;
    }

    public double[][] getIdeals() {
        double[][] result = new double[imagesData.size()][imagesData.get(0).getData().length];
        for(int i = 0; i < imagesData.size(); ++i) {
            double[] ideals = new double[10];

            for(int j = 0; j < 10; ++j) ideals[j] = 0;
            ideals[imagesData.get(i).getLabel()] = 1;

            result[i] = ideals;
        }
        return result;
    }
}