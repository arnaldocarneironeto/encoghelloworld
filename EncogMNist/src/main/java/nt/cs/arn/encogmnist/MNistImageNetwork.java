package nt.cs.arn.encogmnist;

import java.io.File;
import org.encog.Encog;
import org.encog.engine.network.activation.ActivationElliott;
import org.encog.engine.network.activation.ActivationSigmoid;
import org.encog.engine.network.activation.ActivationSteepenedSigmoid;
import org.encog.engine.network.activation.ActivationStep;
import org.encog.ml.data.MLDataSet;
import org.encog.ml.data.basic.BasicMLDataSet;
import org.encog.neural.networks.BasicNetwork;
import org.encog.neural.networks.layers.BasicLayer;
import org.encog.neural.networks.training.Train;
import org.encog.neural.networks.training.propagation.resilient.ResilientPropagation;
import org.encog.persist.EncogDirectoryPersistence;
import org.encog.util.simple.EncogUtility;

/**
 *
 * @author arnaldo
 */
public class MNistImageNetwork {
//    private static final double TARGET_ERROR = 0.001;

    private final double input[][];
    private final double ideal[][];

    private BasicNetwork network;

    public MNistImageNetwork(MNistDataReader reader) {
        this(reader.getData(), reader.getIdeals());
    }

    public MNistImageNetwork(double[][] input, double[][] ideal) {
        this.input = input;
        this.ideal = ideal;
    }

    public void doTtraining() {
        MLDataSet trainingSet = new BasicMLDataSet(input, ideal);

        File networkFile = new File("MNistNN.eg");
        if (!networkFile.exists()) {
            network = new BasicNetwork();
            network.addLayer(new BasicLayer(new ActivationSteepenedSigmoid(), false, input[0].length));
            network.addLayer(new BasicLayer(new ActivationSteepenedSigmoid(), true, 400));
            network.addLayer(new BasicLayer(new ActivationSteepenedSigmoid(), true, 25));
            network.addLayer(new BasicLayer(new ActivationSteepenedSigmoid(), true, ideal[0].length));
            network.getStructure().finalizeStructure();
            network.reset();
        } else {
            network = (BasicNetwork) EncogDirectoryPersistence.loadObject(networkFile);
        }

        Train train = new ResilientPropagation(network, trainingSet); 

//        EncogUtility.trainToError(train, target_error);
        EncogUtility.trainConsole(network, trainingSet, 480);

        EncogDirectoryPersistence.saveObject(networkFile, network);

        Encog.getInstance().shutdown();
    }

    public void load() {
        File networkFile = new File("MNistNN.eg");
        if (networkFile.exists()) {
            network = (BasicNetwork) EncogDirectoryPersistence.loadObject(networkFile);
        }
    }

    public double[] doCompute(double[] singleInput) {
        double[] singleOutput = new double[10];

        network.compute(singleInput, singleOutput);

        return singleOutput;
    }
}